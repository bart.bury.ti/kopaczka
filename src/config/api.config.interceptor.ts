import {Injectable} from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ConfigService} from "./config.service";

@Injectable()
export class ApiConfigInterceptor implements HttpInterceptor {

  constructor(private configService: ConfigService) {
  }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const apiUrl = this.configService.apiUrl;

    const apiReq = request.clone({url: `${apiUrl}/${request.url}`});
    return next.handle(apiReq);
  }
}
