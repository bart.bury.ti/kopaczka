import {Injectable} from '@angular/core';
import {
  AuthenticationDetails,
  CognitoUser,
  CognitoUserAttribute,
  CognitoUserPool, CognitoUserSession,
  ISignUpResult
} from "amazon-cognito-identity-js";
import {AuthSignUp} from "./auth-sign-up.model";
import {Observable, of} from "rxjs";
import {AuthSignIn} from "./auth-sign-in.model";
import {AuthVerificationCode} from "./auth-verification-code.model";

const poolData = {
  UserPoolId: 'eu-west-1_V74btJ5S9',
  ClientId: '6k6pp790457sdn48jtbnrba8r6'
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  signUp(user: AuthSignUp): Observable<ISignUpResult> {
    const userPool = new CognitoUserPool(poolData);

    const attribute = [
      new CognitoUserAttribute({
        Name: 'name',
        Value: user.name,
      }),
      new CognitoUserAttribute({
        Name: 'email',
        Value: user.email,
      }),
    ];

    return new Observable(subscriber => {
      userPool.signUp(user.email, user.password, attribute, attribute, (err, result) => {
        err
          ? subscriber.error(err.name)
          : subscriber.next(result);

        subscriber.complete();
      })
    })
  }

  signIn(user: AuthSignIn): Observable<CognitoUserSession> {
    return new Observable(subscriber => {
      this.createCognitoUser(user.email).authenticateUser(new AuthenticationDetails({
        Username: user.email,
        Password: user.password,
      }), {
        onSuccess: (session, userConfirmationNecessary) => {
          subscriber.next(session);
          subscriber.complete();
        },
        onFailure: err => {
          subscriber.error(err.name);
          subscriber.complete();
        },
      })
    })
  }

  verification(user: AuthVerificationCode) {
    return new Observable(subscriber => {
      this.createCognitoUser(user.email).confirmRegistration(user.code, true, (err, result) => {
        err
          ? subscriber.error(err)
          : subscriber.next(result);

        subscriber.complete();
      });
    });
  }

  reSignIn(): Observable<CognitoUserSession | null> {
    const userPool = new CognitoUserPool(poolData);
    const cognitoUser = userPool.getCurrentUser();

    if (cognitoUser === null) {
      return of(null);
    }

    return new Observable(subscriber => {
      cognitoUser.getSession((error: Error | null, session: CognitoUserSession | null) => {

        session
          ? subscriber.next(session)
          : subscriber.error(error);

        subscriber.complete();
      })
    })
  }

  private createCognitoUser(email: string): CognitoUser {
    const userPool = new CognitoUserPool(poolData);

    const userData = {
      Username: email,
      Pool: userPool,
    };

    return new CognitoUser(userData);
  }
}
