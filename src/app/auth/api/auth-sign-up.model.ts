export interface AuthSignUp {
  name: string
  email: string
  password: string
}
