export interface AuthSignIn {
  email: string
  password: string
}
