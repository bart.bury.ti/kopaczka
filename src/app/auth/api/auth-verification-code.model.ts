export interface AuthVerificationCode {
  email: string
  code: string
}
