import {Injectable} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthSignIn} from "../api/auth-sign-in.model";

interface SignInForm {
  email: FormControl<string>,
  password: FormControl<string>,
}

@Injectable()
export class AuthSignInFormService {

  form = new FormGroup<SignInForm>({
    email: new FormControl('', {nonNullable: true, validators: [Validators.required]}),
    password: new FormControl('', {nonNullable: true, validators: [Validators.required]}),
  })

  getValue(): AuthSignIn {
    return {
      email: this.form.value.email!,
      password: this.form.value.password!
    }
  }
}
