import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthSignInComponent } from './auth-sign-in.component';
import {AuthLogoModule} from "../components/logo/auth-logo.module";
import {WrapperFormModule} from "../../shared/wrapper-form/wrapper-form.module";
import {ReactiveFormsModule} from "@angular/forms";
import {InputModule} from "../../shared/input/input.module";
import {ButtonSolidModule} from "../../shared/button-solid/button-solid.module";

@NgModule({
  declarations: [
    AuthSignInComponent
  ],
  imports: [
    CommonModule,
    AuthLogoModule,
    WrapperFormModule,
    ReactiveFormsModule,
    InputModule,
    ButtonSolidModule
  ]
})
export class AuthSignInModule { }
