import {Component} from '@angular/core';
import {AuthSignInFormService} from "./auth-sign-in-form.service";
import {AuthService} from "../api/auth.service";
import {UserFacade} from "../../user/user.facade";
import {CognitoUserSession} from "amazon-cognito-identity-js";
import {ActivatedRoute, Router} from "@angular/router";
import {authRoutingPath} from "../auth.routing";
import {catchError, of, throwError} from "rxjs";

@Component({
  selector: 'app-auth-sign-in',
  templateUrl: './auth-sign-in.component.html',
  styleUrls: ['./auth-sign-in.component.scss'],
  providers: [AuthSignInFormService],
})
export class AuthSignInComponent {

  form = this.authSignInFormService.form;

  constructor(
    private authSignInFormService: AuthSignInFormService,
    private authService: AuthService,
    private userFacade: UserFacade,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
  }

  signIn() {
    this.authService.signIn(this.authSignInFormService.getValue()).pipe(
      catchError(err => {
        switch (err) {
          case 'UserNotConfirmedException':
            this.userConfirmationNecessary();
            break;
        }

        return throwError(() => err)
      })
    ).subscribe(user => this.userLoggedIn(user));
  }

  private userLoggedIn(user: CognitoUserSession) {
    this.userFacade.updateCognitoUser(user);
    this.router.navigate!(['.']);
  }

  private userConfirmationNecessary() {
    this.router.navigate!(['../' + authRoutingPath.verificationCode], {relativeTo: this.activatedRoute});
  }
}
