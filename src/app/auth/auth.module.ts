import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {AuthRouting} from './auth.routing';
import {AuthSplitModule} from "./split/auth-split.module";
import {AuthSignUpModule} from "./sign-up/auth-sign-up.module";
import {AuthSignInModule} from "./sign-in/auth-sign-in.module";
import {AuthVerificationCodeModule} from "./verification-code/auth-verification-code.module";


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AuthRouting,
    AuthSplitModule,
    AuthSignUpModule,
    AuthSignInModule,
    AuthVerificationCodeModule,
  ]
})
export class AuthModule {
}
