import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthSplitComponent} from "./split/auth-split.component";
import {AuthSignUpComponent} from "./sign-up/auth-sign-up.component";
import {AuthSignInComponent} from "./sign-in/auth-sign-in.component";
import {AuthVerificationCodeComponent} from "./verification-code/auth-verification-code.component";

export const authRoutingPath = {
  split: '',
  signUp: 'sign-up',
  signIn: 'sign-in',
  verificationCode: 'verification-code'
}

const routes: Routes = [
  {
    path: authRoutingPath.split,
    component: AuthSplitComponent,
  },
  {
    path: authRoutingPath.signUp,
    component: AuthSignUpComponent,
  },
  {
    path: authRoutingPath.signIn,
    component: AuthSignInComponent,
  },
  {
    path: authRoutingPath.verificationCode,
    component: AuthVerificationCodeComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRouting {
}
