import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthLogoComponent } from './auth-logo.component';
import {SvgModule} from "../../../shared/svg/svg.module";



@NgModule({
  declarations: [
    AuthLogoComponent
  ],
  exports: [
    AuthLogoComponent
  ],
  imports: [
    CommonModule,
    SvgModule
  ]
})
export class AuthLogoModule { }
