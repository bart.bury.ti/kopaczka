import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {UserFacade} from "../user/user.facade";
import {Observable, switchMap} from "rxjs";

@Injectable()
export class AuthTokenInterceptor implements HttpInterceptor {
  constructor(private userFacade: UserFacade) {
  }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return this.userFacade.user$.pipe(
      switchMap(user => {
        request = request.clone({
          setHeaders: {
            Authorization: `Bearer ${user.cognito.getAccessToken().getJwtToken()}`,
          }
        })

        return next.handle(request)
      })
    )
  }
}
