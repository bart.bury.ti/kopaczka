import {Injectable} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthVerificationCode} from "../api/auth-verification-code.model";

interface VerificationCodeForm {
  email: FormControl<string>,
  code: FormControl<string>,
}

@Injectable()
export class AuthVerificationCodeFormService {

  form = new FormGroup<VerificationCodeForm>({
    email: new FormControl('', {nonNullable: true, validators: [Validators.required]}),
    code: new FormControl('', {nonNullable: true, validators: [Validators.required]}),
  })

  getValue(): AuthVerificationCode {
    return {
      code: this.form.value.code!,
      email: this.form.value.email!
    }
  }
}
