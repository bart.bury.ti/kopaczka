import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthVerificationCodeComponent } from './auth-verification-code.component';
import {AuthLogoModule} from "../components/logo/auth-logo.module";
import {WrapperFormModule} from "../../shared/wrapper-form/wrapper-form.module";
import {ButtonSolidModule} from "../../shared/button-solid/button-solid.module";
import {InputModule} from "../../shared/input/input.module";
import {ReactiveFormsModule} from "@angular/forms";



@NgModule({
  declarations: [
    AuthVerificationCodeComponent
  ],
  imports: [
    CommonModule,
    AuthLogoModule,
    WrapperFormModule,
    ButtonSolidModule,
    InputModule,
    ReactiveFormsModule
  ]
})
export class AuthVerificationCodeModule { }
