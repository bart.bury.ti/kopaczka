import {Component} from '@angular/core';
import {AuthVerificationCodeFormService} from "./auth-verification-code-form.service";
import {AuthService} from "../api/auth.service";
import {authRoutingPath} from "../auth.routing";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-auth-verification-code',
  templateUrl: './auth-verification-code.component.html',
  styleUrls: ['./auth-verification-code.component.scss'],
  providers: [AuthVerificationCodeFormService]
})
export class AuthVerificationCodeComponent {

  form = this.authVerificationCodeFormService.form;

  constructor(
    private authVerificationCodeFormService: AuthVerificationCodeFormService,
    private authService: AuthService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
  }

  verification() {
    this.authService.verification(this.authVerificationCodeFormService.getValue()).subscribe(() => this.verified())
  }

  private verified() {
    this.router.navigate!(['../' + authRoutingPath.signIn], {relativeTo: this.activatedRoute});
  }
}
