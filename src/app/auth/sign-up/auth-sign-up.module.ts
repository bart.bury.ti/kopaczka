import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthSignUpComponent } from './auth-sign-up.component';
import {AuthLogoModule} from "../components/logo/auth-logo.module";
import {ReactiveFormsModule} from "@angular/forms";
import {InputModule} from "../../shared/input/input.module";
import {ButtonSolidModule} from "../../shared/button-solid/button-solid.module";
import {WrapperFormModule} from "../../shared/wrapper-form/wrapper-form.module";



@NgModule({
  declarations: [
    AuthSignUpComponent
  ],
  imports: [
    CommonModule,
    AuthLogoModule,
    ReactiveFormsModule,
    InputModule,
    ButtonSolidModule,
    WrapperFormModule
  ]
})
export class AuthSignUpModule { }
