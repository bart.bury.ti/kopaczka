import {Injectable} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthSignUp} from "../api/auth-sign-up.model";

interface SignUpForm {
  name: FormControl<string>,
  email: FormControl<string>,
  password: FormControl<string>,
  repeatPassword: FormControl<string>,
}

@Injectable()
export class AuthSignUpFormService {

  form = new FormGroup<SignUpForm>({
    name: new FormControl('', {nonNullable: true, validators: [Validators.required]}),
    email: new FormControl('', {nonNullable: true, validators: [Validators.required]}),
    password: new FormControl('', {nonNullable: true, validators: [Validators.required]}),
    repeatPassword: new FormControl('', {nonNullable: true, validators: [Validators.required]}),
  })

  getValue(): AuthSignUp {
    return {
      name: this.form.value.name!,
      email: this.form.value.email!,
      password: this.form.value.password!,
    }
  }
}
