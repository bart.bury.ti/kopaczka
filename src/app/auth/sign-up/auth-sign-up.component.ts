import {Component} from '@angular/core';
import {AuthSignUpFormService} from "./auth-sign-up-form.service";
import {AuthService} from "../api/auth.service";
import {ActivatedRoute, Router} from "@angular/router";
import {authRoutingPath} from "../auth.routing";

@Component({
  selector: 'app-auth-sign-up',
  templateUrl: './auth-sign-up.component.html',
  styleUrls: ['./auth-sign-up.component.scss'],
  providers: [AuthSignUpFormService],
})
export class AuthSignUpComponent {

  form = this.authSignUpFormService.form;

  constructor(
    private authSignUpFormService: AuthSignUpFormService,
    private authService: AuthService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
  }

  signUp() {
    this.authService.signUp(this.authSignUpFormService.getValue()).subscribe(() => this.registeredUser());
  }

  private registeredUser() {
    this.router.navigate!(['../' + authRoutingPath.verificationCode], {relativeTo: this.activatedRoute});
  }
}
