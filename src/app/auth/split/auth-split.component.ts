import {Component} from '@angular/core';
import {authRoutingPath} from "../auth.routing";

@Component({
  selector: 'app-auth-split',
  templateUrl: './auth-split.component.html',
  styleUrls: ['./auth-split.component.scss']
})
export class AuthSplitComponent {
  routerLink = authRoutingPath;
}
