import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthSplitComponent } from './auth-split.component';
import {HeaderH3Module} from "../../shared/header-h3/header-h3.module";
import {ButtonSolidModule} from "../../shared/button-solid/button-solid.module";
import {AuthLogoModule} from "../components/logo/auth-logo.module";
import {RouterLink} from "@angular/router";



@NgModule({
  declarations: [
    AuthSplitComponent
  ],
  imports: [
    CommonModule,
    HeaderH3Module,
    ButtonSolidModule,
    AuthLogoModule,
    RouterLink
  ]
})
export class AuthSplitModule { }
