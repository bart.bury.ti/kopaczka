import {Observable, tap} from "rxjs";
import {AuthService} from "./api/auth.service";
import {UserFacade} from "../user/user.facade";
import {Router} from "@angular/router";
import {appRoutingPath} from "../app.routing";

export function authInitialize(authService: AuthService, userFacade: UserFacade, router: Router): () => Observable<any> {
  return () => authService.reSignIn().pipe(
    tap(cognito => cognito
      ? userFacade.updateCognitoUser(cognito)
      : router.navigate!([appRoutingPath.auth])
    )
  )
}
