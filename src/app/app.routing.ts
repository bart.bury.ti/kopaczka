import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {WrapperDashboardComponent} from "./shared/wrapper-dashboard/wrapper-dashboard.component";
import {WrapperAuthComponent} from "./shared/wrapper-auth/wrapper-auth.component";
import {dashboardRoutingPath} from "./dashboard/dashboard.routing";

export const appRoutingPath = {
  dashboard: '',
  auth: 'auth'
}

const routes: Routes = [
  {
    path: '',
    component: WrapperDashboardComponent,
    children: [
      {
        path: appRoutingPath.dashboard,
        loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule),
        title: 'Dashboard'
      },
      {
        path: dashboardRoutingPath.parcel + '/:id',
        loadChildren: () => import('./parcel/parcel.module').then(m => m.ParcelModule)
      }
    ]
  },
  {
    path: appRoutingPath.auth,
    component: WrapperAuthComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule),
        title: 'Auth',
      }
    ]
  },
  {
    path: "**", redirectTo: "",
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRouting {
}
