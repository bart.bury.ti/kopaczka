import {Component} from '@angular/core';
import {ParcelFacade} from "../parcel.facade";
import {switchMap, take} from "rxjs";
import {ParcelUpdateDeliveryDateFormService} from "./parcel-update-delivery-date-form.service";
import {ModalCenterService} from "../../shared/modal-center/modal-center.service";

@Component({
  selector: 'app-parcel-update-delivery-date',
  templateUrl: './parcel-update-delivery-date.component.html',
  styleUrls: ['./parcel-update-delivery-date.component.scss'],
  providers: [ParcelUpdateDeliveryDateFormService]
})
export class ParcelUpdateDeliveryDateComponent {
  form$ = this.parcelUpdateDeliveryDateFormService.getFormWithData$();

  constructor(
    private parcelFacade: ParcelFacade,
    private modalCenterService: ModalCenterService,
    private parcelUpdateDeliveryDateFormService: ParcelUpdateDeliveryDateFormService
  ) {
  }

  update() {
    this.parcelFacade.currentParcel$.pipe(
      take(1),
      switchMap(parcel => this.parcelFacade.updateParcelDeliveryDate$(parcel.uuid, this.parcelUpdateDeliveryDateFormService.getValue()))
    ).subscribe(() => this.modalCenterService.close());
  }

  cancel() {
    this.modalCenterService.close();
  }
}
