import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ParcelUpdateDeliveryDateComponent} from './parcel-update-delivery-date.component';
import {DatePickerModule} from "../../shared/date-picker/date-picker.module";
import {ReactiveFormsModule} from "@angular/forms";
import {HeaderH2Module} from "../../shared/header-h2/header-h2.module";


@NgModule({
  declarations: [
    ParcelUpdateDeliveryDateComponent
  ],
  imports: [
    CommonModule,
    DatePickerModule,
    ReactiveFormsModule,
    HeaderH2Module
  ]
})
export class ParcelUpdateDeliveryDateModule {
}
