import {Injectable} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {ParcelFacade} from "../parcel.facade";
import {map} from "rxjs";
import {ParcelUpdateDeliveryDate} from "../api/parcel-update-delivery-date";
import {format} from "date-fns";

interface ParcelUpdateDeliveryDateForm {
  estimatedDeliveryDate: FormControl<Date | null>
}

@Injectable()
export class ParcelUpdateDeliveryDateFormService {

  form = new FormGroup<ParcelUpdateDeliveryDateForm>({
    estimatedDeliveryDate: new FormControl(null)
  })

  constructor(
    private parcelFacade: ParcelFacade,
  ) {
  }

  getValue(): ParcelUpdateDeliveryDate {
    return {
      estimatedDeliveryDate: format(this.form.value.estimatedDeliveryDate!, 'yyyy-MM-dd')
    }
  }

  getFormWithData$() {
    return this.parcelFacade.currentParcel$.pipe(
      map(parcel => {
        this.form.patchValue({
          estimatedDeliveryDate: new Date(parcel.estimatedDeliveryDate)
        })

        return this.form;
      })
    )
  }
}
