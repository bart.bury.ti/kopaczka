import {RealizationStatus} from "./enums/parcel-realization-status.enum";
import {ParcelSize} from "./enums/parcel-size.enum";

export interface Parcel {
  uuid: string
  refNumber: string
  name: string
  carrier: {
    name: string
    phone: string
  }
  dimensions: {
    height: number
    id: number
    length: number
    size: ParcelSize
    weight: number
    width: number
  }
  pickUpDetails?: {
    openLockerUrl: string
    pickupTimeLeftInHours: number
    pin: string
  }
  estimatedDeliveryDate: string
  sender: {
    companyName: string
    address: Address
  }
  receiver: {
    name: string
    address: Address
    phone: string
  }
  realizationStatus: RealizationStatus
  waypoints: Waypoint[]
}

export interface Address {
  city: string
  flatNumber: string
  postalCode: string
  street: string
  streetNumber: string
}

export interface Waypoint {
  city: string
  date: string
  time: string
  title: string
}
