import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {Parcel} from "./parcel.model";
import {HttpClient} from "@angular/common/http";
import {ParcelCreate} from "./parcel-create.model";
import {ParcelUpdateDeliveryAddress} from "./parcel-update-delivery-address";
import {ParcelUpdateDeliveryDate} from "./parcel-update-delivery-date";

/**
 * @Internal use DashboardFacade
 */
@Injectable({
  providedIn: 'root'
})
export class ParcelService {
  private readonly base = 'parcels';

  constructor(private httpClient: HttpClient) {
  }

  getParcels$(): Observable<Parcel[]> {
    return this.httpClient.get<Parcel[]>(this.base);
  }

  getParcel$(uuid: string): Observable<Parcel> {
    return this.httpClient.get<Parcel>(this.base + `/${uuid}`);
  }

  createParcel$(parcel: ParcelCreate): Observable<Parcel> {
    return this.httpClient.post<Parcel>(this.base, parcel);
  }

  updateParcelDeliveryAddress$(uuidParcel: string, parcelUpdateDeliveryAddress: ParcelUpdateDeliveryAddress): Observable<Parcel> {
    return this.httpClient.put<Parcel>(`${this.base}/${uuidParcel}/deliveryAddress`, parcelUpdateDeliveryAddress);
  }

  updateParcelDeliveryDate$(uuidParcel: string, parcelUpdateDeliveryDate: ParcelUpdateDeliveryDate): Observable<Parcel> {
    return this.httpClient.put<Parcel>(`${this.base}/${uuidParcel}/date`, parcelUpdateDeliveryDate);
  }
}
