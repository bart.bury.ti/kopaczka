export enum RealizationStatus {
  inRealization = 'IN_REALIZATION',
  readyForPickup = 'READY_FOR_PICKUP',
  finished = 'FINISHED',
  expired = 'EXPIRED',
}
