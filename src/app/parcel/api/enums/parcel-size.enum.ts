export enum ParcelSize {
  s = 'S',
  m = 'M',
  l = 'L',
  XL = 'XL'
}
