export interface ParcelUpdateDeliveryDate {
  estimatedDeliveryDate: string
}
