export interface ParcelUpdateDeliveryAddress {
  street: string
  city: string
  postalCode: string
  streetNumber: string
  flatNumber: string
}
