import {Component, OnInit} from '@angular/core';
import {ParcelFacade} from "../parcel.facade";
import {ActivatedRoute} from "@angular/router";
import {ModalService} from "../../shared/modal/modal.service";
import {
  ParcelUpdateDeliveryAddressComponent
} from "../update-delivery-address/parcel-update-delivery-address.component";
import {ParcelUpdateDeliveryDateComponent} from "../update-delivery-date/parcel-update-delivery-date.component";
import {ModalCenterService} from "../../shared/modal-center/modal-center.service";

@Component({
  selector: 'app-parcel-view',
  templateUrl: './parcel-view.component.html',
  styleUrls: ['./parcel-view.component.scss']
})
export class ParcelViewComponent {
  parcel$ = this.dashboardFacade.getParcel$(this.uuid);

  constructor(
    private dashboardFacade: ParcelFacade,
    private activatedRoute: ActivatedRoute,
    private modalService: ModalService,
    private modalCenterService: ModalCenterService
  ) {
  }

  get uuid(): string {
    return this.activatedRoute.snapshot.paramMap.get('id')!;
  }

  updateDeliveryAddress() {
    this.modalService.open(ParcelUpdateDeliveryAddressComponent);
  }

  updateDeliveryDate() {
    this.modalCenterService.open(ParcelUpdateDeliveryDateComponent);
  }
}
