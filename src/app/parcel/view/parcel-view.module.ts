import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ParcelViewComponent} from './parcel-view.component';
import {ParcelWaypointsModule} from "./waypoints/parcel-waypoints.module";
import {ParcelSizeModule} from "./size/parcel-size.module";
import {SvgModule} from "../../shared/svg/svg.module";
import {PackageStatusModule} from "../../dashboard/components/package-status/package-status.module";
import {ParcelQrModule} from "./qr/parcel-qr.module";
import {HeaderH2Module} from "../../shared/header-h2/header-h2.module";
import {HeaderH3Module} from "../../shared/header-h3/header-h3.module";
import {LinkModule} from "../../shared/link/link.module";
import {FullStreetAddressPipe} from "../../dashboard/pipes/full-street-address.pipe";
import {ParcelDimensionsPipe} from "../../dashboard/pipes/parcel-dimensions.pipe";
import {RouterLink} from "@angular/router";

@NgModule({
  declarations: [
    ParcelViewComponent
  ],
  imports: [
    CommonModule,
    ParcelWaypointsModule,
    ParcelSizeModule,
    SvgModule,
    PackageStatusModule,
    ParcelQrModule,
    HeaderH2Module,
    HeaderH3Module,
    LinkModule,
    FullStreetAddressPipe,
    ParcelDimensionsPipe,
    RouterLink,
  ]
})
export class ParcelViewModule {
}
