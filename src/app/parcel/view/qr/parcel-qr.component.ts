import {Component} from '@angular/core';
import {ParcelFacade} from "../../parcel.facade";

@Component({
  selector: 'app-parcel-qr',
  templateUrl: './parcel-qr.component.html',
  styleUrls: ['./parcel-qr.component.scss']
})
export class ParcelQrComponent {
  parcel$ = this.parcelFacade.currentParcel$;

  constructor(private parcelFacade: ParcelFacade) {
  }
}
