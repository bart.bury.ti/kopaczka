import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ParcelQrComponent } from './parcel-qr.component';
import {HeaderH2Module} from "../../../shared/header-h2/header-h2.module";
import {ButtonSolidModule} from "../../../shared/button-solid/button-solid.module";



@NgModule({
    declarations: [
        ParcelQrComponent
    ],
    exports: [
        ParcelQrComponent
    ],
  imports: [
    CommonModule,
    HeaderH2Module,
    ButtonSolidModule
  ]
})
export class ParcelQrModule { }
