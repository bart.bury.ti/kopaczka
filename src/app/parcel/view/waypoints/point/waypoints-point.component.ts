import {Component, HostBinding, Input} from '@angular/core';

@Component({
  selector: 'app-waypoints-point',
  templateUrl: './waypoints-point.component.html',
  styleUrls: ['./waypoints-point.component.scss']
})
export class WaypointsPointComponent {
  @HostBinding('attr.color')
  @Input() color: 'secondary' | 'tertiary' | undefined;
}
