import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ParcelWaypointsComponent} from './parcel-waypoints.component';
import {HeaderH3Module} from "../../../shared/header-h3/header-h3.module";
import { WaypointsPointComponent } from './point/waypoints-point.component';
import { WaypointsWaypointComponent } from './waypoint/waypoints-waypoint.component';


@NgModule({
  declarations: [
    ParcelWaypointsComponent,
    WaypointsPointComponent,
    WaypointsWaypointComponent,
  ],
  exports: [
    ParcelWaypointsComponent
  ],
  imports: [
    CommonModule,
    HeaderH3Module
  ]
})
export class ParcelWaypointsModule {
}
