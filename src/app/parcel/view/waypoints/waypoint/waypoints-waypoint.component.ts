import {Component, Input} from '@angular/core';
import {Parcel, Waypoint} from "../../../api/parcel.model";

@Component({
  selector: 'app-waypoints-waypoint',
  templateUrl: './waypoints-waypoint.component.html',
  styleUrls: ['./waypoints-waypoint.component.scss']
})
export class WaypointsWaypointComponent {
  @Input() waypoint!: Partial<Waypoint>;
  @Input() status!: Parcel['realizationStatus'] | 'COMING';
  @Input() specialDesignByStatus = false;
}
