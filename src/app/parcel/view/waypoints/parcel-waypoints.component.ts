import {Component} from '@angular/core';
import {ParcelFacade} from "../../parcel.facade";
import {map} from "rxjs";
import {Waypoint} from "../../api/parcel.model";

@Component({
  selector: 'app-parcel-waypoints',
  templateUrl: './parcel-waypoints.component.html',
  styleUrls: ['./parcel-waypoints.component.scss']
})
export class ParcelWaypointsComponent {
  data$ = this.parcelFacade.currentParcel$.pipe(
    map(parcel => {

      const waypoint: Partial<Waypoint> = {
        date: parcel.estimatedDeliveryDate,
        title: 'Spodziewany termin dostawy',
        city: parcel.receiver.address.city
      }

      return {
        waypoints: parcel.waypoints,
        status: parcel.realizationStatus,
        waypoint
      }
    })
  )

  constructor(private parcelFacade: ParcelFacade) {
  }
}
