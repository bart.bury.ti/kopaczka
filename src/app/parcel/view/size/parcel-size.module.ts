import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ParcelSizeComponent } from './parcel-size.component';
import {SvgModule} from "../../../shared/svg/svg.module";
import {HeaderH3Module} from "../../../shared/header-h3/header-h3.module";



@NgModule({
  declarations: [
    ParcelSizeComponent
  ],
  exports: [
    ParcelSizeComponent
  ],
  imports: [
    CommonModule,
    SvgModule,
    HeaderH3Module
  ]
})
export class ParcelSizeModule { }
