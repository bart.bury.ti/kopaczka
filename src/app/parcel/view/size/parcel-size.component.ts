import {Component, HostBinding, Input} from '@angular/core';

@Component({
  selector: 'app-parcel-size',
  templateUrl: './parcel-size.component.html',
  styleUrls: ['./parcel-size.component.scss']
})
export class ParcelSizeComponent {
  @Input() size: 'S' | 'M' | 'L' | 'XL' | undefined;

  @HostBinding('attr.active')
  @Input() active: false | true | undefined;
}
