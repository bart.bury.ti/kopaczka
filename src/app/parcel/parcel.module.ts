import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ParcelRouting} from './parcel.routing';
import {ParcelViewModule} from "./view/parcel-view.module";
import {ParcelUpdateDeliveryAddressModule} from "./update-delivery-address/parcel-update-delivery-address.module";
import {ParcelUpdateDeliveryDateModule} from "./update-delivery-date/parcel-update-delivery-date.module";

@NgModule({
  imports: [
    CommonModule,
    ParcelRouting,
    ParcelViewModule,
    ParcelUpdateDeliveryAddressModule,
    ParcelUpdateDeliveryDateModule,
  ]
})
export class ParcelModule {
}
