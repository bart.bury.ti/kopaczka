import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ParcelUpdateDeliveryAddressComponent} from './parcel-update-delivery-address.component';
import {WrapperFormModule} from "../../shared/wrapper-form/wrapper-form.module";
import {ReactiveFormsModule} from "@angular/forms";
import {InputModule} from "../../shared/input/input.module";
import {ButtonSolidModule} from "../../shared/button-solid/button-solid.module";

@NgModule({
  declarations: [
    ParcelUpdateDeliveryAddressComponent
  ],
  imports: [
    CommonModule,
    WrapperFormModule,
    ReactiveFormsModule,
    InputModule,
    ButtonSolidModule
  ]
})
export class ParcelUpdateDeliveryAddressModule {
}
