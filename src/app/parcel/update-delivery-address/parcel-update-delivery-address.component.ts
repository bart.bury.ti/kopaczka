import {Component} from '@angular/core';
import {ParcelUpdateDeliveryAddressFormService} from "./parcel-update-delivery-address-form.service";
import {ParcelFacade} from "../parcel.facade";
import {switchMap, take} from "rxjs";
import {ModalService} from "../../shared/modal/modal.service";

@Component({
  selector: 'app-parcel-update-delivery-address',
  templateUrl: './parcel-update-delivery-address.component.html',
  styleUrls: ['./parcel-update-delivery-address.component.scss'],
  providers: [ParcelUpdateDeliveryAddressFormService]
})
export class ParcelUpdateDeliveryAddressComponent {

  form$ = this.parcelUpdateDeliveryAddressFormService.getFormWithData$();

  constructor(
    private parcelUpdateDeliveryAddressFormService: ParcelUpdateDeliveryAddressFormService,
    private parcelFacade: ParcelFacade,
    private modalService: ModalService
  ) {
  }

  update() {
    this.parcelFacade.currentParcel$.pipe(
      take(1),
      switchMap(parcel => this.parcelFacade.updateParcelDeliveryAddress$(parcel.uuid, this.parcelUpdateDeliveryAddressFormService.getValue()))
    ).subscribe(() => this.modalService.close());
  }
}
