import {Injectable} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ParcelUpdateDeliveryAddress} from "../api/parcel-update-delivery-address";
import {ParcelFacade} from "../parcel.facade";
import {Observable, of, switchMap} from "rxjs";

interface ParcelUpdateDeliveryAddressForm {
  street: FormControl<string>
  city: FormControl<string>
  postalCode: FormControl<string>
  streetNumber: FormControl<string>
  flatNumber: FormControl<string>
}

@Injectable()
export class ParcelUpdateDeliveryAddressFormService {

  private form = new FormGroup<ParcelUpdateDeliveryAddressForm>({
    street: new FormControl('', {nonNullable: true, validators: [Validators.required]}),
    city: new FormControl('', {nonNullable: true, validators: [Validators.required]}),
    postalCode: new FormControl('', {nonNullable: true, validators: [Validators.required]}),
    streetNumber: new FormControl('', {nonNullable: true, validators: [Validators.required]}),
    flatNumber: new FormControl('', {nonNullable: true, validators: [Validators.required]}),
  })

  constructor(private parcelFacade: ParcelFacade) {
  }

  getFormWithData$(): Observable<FormGroup<ParcelUpdateDeliveryAddressForm>> {
    return this.parcelFacade.currentParcel$.pipe(
      switchMap(parcel => {
        this.form.patchValue(parcel.receiver.address);
        return of(this.form);
      })
    )
  }

  getValue(): ParcelUpdateDeliveryAddress {
    return {
      street: this.form.value.street!,
      city: this.form.value.city!,
      postalCode: this.form.value.postalCode!,
      streetNumber: this.form.value.streetNumber!,
      flatNumber: this.form.value.flatNumber!,
    }
  }
}
