import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ParcelCreateComponent} from './parcel-create.component';
import {ReactiveFormsModule} from "@angular/forms";
import {WrapperFormModule} from "../../shared/wrapper-form/wrapper-form.module";
import {ButtonSolidModule} from "../../shared/button-solid/button-solid.module";
import {InputModule} from "../../shared/input/input.module";

@NgModule({
  declarations: [
    ParcelCreateComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    WrapperFormModule,
    ButtonSolidModule,
    InputModule
  ]
})
export class ParcelCreateModule {
}
