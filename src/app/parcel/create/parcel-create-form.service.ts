import {Inject, Injectable} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ParcelCreate} from "../api/parcel-create.model";
import {MODAL_EXTERNAL_DATA} from "../../shared/modal/modal-token.injection";

interface ParcelCreateForm {
  refNumber: FormControl<string>,
  carrierName: FormControl<string>,
}

@Injectable()
export class ParcelCreateFormService {

  form = new FormGroup<ParcelCreateForm>({
    refNumber: new FormControl(this.data?.refNumber ?? '', {nonNullable: true, validators: [Validators.required]}),
    carrierName: new FormControl(this.data?.carrierName ?? '', {nonNullable: true, validators: [Validators.required]}),
  })

  constructor(@Inject(MODAL_EXTERNAL_DATA) private data?: { refNumber: string, carrierName: string }) {
  }

  getValue(): ParcelCreate {
    return {
      refNumber: this.form.value.refNumber!,
      carrierName: this.form.value.carrierName!,
    }
  }
}
