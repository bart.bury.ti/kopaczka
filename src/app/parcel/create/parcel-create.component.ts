import {Component} from '@angular/core';
import {ParcelCreateFormService} from "./parcel-create-form.service";
import {ParcelFacade} from "../parcel.facade";
import {ModalService} from "../../shared/modal/modal.service";

@Component({
  selector: 'app-parcel-create',
  templateUrl: './parcel-create.component.html',
  styleUrls: ['./parcel-create.component.scss'],
  providers: [ParcelCreateFormService]
})
export class ParcelCreateComponent {

  form = this.parcelCreateFormService.form;

  constructor(
    private parcelCreateFormService: ParcelCreateFormService,
    private parcelFacade: ParcelFacade,
    private modalService: ModalService
  ) {
  }

  create() {
    this.parcelFacade.createParcel$(this.parcelCreateFormService.getValue()).subscribe(() => this.modalService.close());
  }
}
