import {Injectable} from '@angular/core';
import {ReplaySubject} from "rxjs";
import {Parcel} from "./api/parcel.model";

/**
 * @Internal use DashboardFacade
 */
@Injectable({
  providedIn: 'root'
})
export class ParcelStore {
  private currentParcel$$ = new ReplaySubject<Parcel>(1);
  currentParcel$ = this.currentParcel$$.asObservable();

  private parcels$$ = new ReplaySubject<Parcel[]>(1);
  parcels$ = this.parcels$$.asObservable();

  updateCurrentParcel(parcel: Parcel) {
    this.currentParcel$$.next(parcel);
  }

  updateParcels(parcels: Parcel[]) {
    this.parcels$$.next(parcels);
  }
}
