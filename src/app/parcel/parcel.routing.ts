import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ParcelViewComponent} from "./view/parcel-view.component";

const routes: Routes = [
  {
    path: '', component: ParcelViewComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ParcelRouting {
}
