import {Injectable} from '@angular/core';
import {Parcel} from "./api/parcel.model";
import {map, Observable, shareReplay, switchMap, tap} from "rxjs";
import {ParcelService} from "./api/parcel.service";
import {ParcelStore} from "./parcel.store";
import {ParcelCreate} from "./api/parcel-create.model";
import {ParcelUpdateDeliveryAddress} from "./api/parcel-update-delivery-address";
import {ParcelUpdateDeliveryDate} from "./api/parcel-update-delivery-date";

@Injectable({
  providedIn: 'root'
})
export class ParcelFacade {
  parcels$ = this.getParcels$().pipe(
    shareReplay(1),
    switchMap(() => this.parcelStore.parcels$)
  )

  currentParcel$ = this.parcelStore.currentParcel$;

  constructor(
    private parcelService: ParcelService,
    private parcelStore: ParcelStore,
  ) {
  }

  getParcel$(uuid: string) {
    return this.parcelService.getParcel$(uuid).pipe(
      tap(parcel => this.parcelStore.updateCurrentParcel(parcel)),
      switchMap(() => this.parcelStore.currentParcel$)
    );
  }

  createParcel$(parcel: ParcelCreate): Observable<Parcel> {
    return this.parcelService.createParcel$(parcel).pipe(
      switchMap(parcel => this.getParcels$().pipe(
        map(() => parcel)
      ))
    );
  }

  updateParcelDeliveryAddress$(uuidParcel: string, parcelUpdateDeliveryAddress: ParcelUpdateDeliveryAddress): Observable<Parcel> {
    return this.parcelService.updateParcelDeliveryAddress$(uuidParcel, parcelUpdateDeliveryAddress).pipe(
      switchMap(parcel => this.getParcel$(uuidParcel).pipe(
        map(() => parcel)
      ))
    )
  }

  updateParcelDeliveryDate$(uuidParcel: string, parcelUpdateDeliveryDate: ParcelUpdateDeliveryDate): Observable<Parcel> {
    return this.parcelService.updateParcelDeliveryDate$(uuidParcel, parcelUpdateDeliveryDate).pipe(
      switchMap(parcel => this.getParcel$(uuidParcel).pipe(
        map(() => parcel)
      ))
    )
  }

  private getParcels$(): Observable<Parcel[]> {
    return this.parcelService.getParcels$().pipe(
      tap(parcels => this.parcelStore.updateParcels(parcels))
    );
  }
}
