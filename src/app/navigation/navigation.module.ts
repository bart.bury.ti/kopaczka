import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NavigationComponent} from './navigation.component';
import {NavigationMenuModule} from "./menu/navigation-menu.module";


@NgModule({
  declarations: [
    NavigationComponent
  ],
  exports: [
    NavigationComponent
  ],
  imports: [
    CommonModule,
    NavigationMenuModule
  ]
})
export class NavigationModule {
}
