import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NavigationMenuComponent} from './navigation-menu.component';
import {SvgModule} from "../../shared/svg/svg.module";
import {ParcelCreateModule} from "../../parcel/create/parcel-create.module";

@NgModule({
  declarations: [
    NavigationMenuComponent
  ],
  exports: [
    NavigationMenuComponent
  ],
  imports: [
    CommonModule,
    SvgModule,
    ParcelCreateModule,
  ]
})
export class NavigationMenuModule {
}
