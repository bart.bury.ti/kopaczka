import {Component, OnInit} from '@angular/core';
import {ModalService} from "../../shared/modal/modal.service";
import {ParcelCreateComponent} from "../../parcel/create/parcel-create.component";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-navigation-menu',
  templateUrl: './navigation-menu.component.html',
  styleUrls: ['./navigation-menu.component.scss']
})
export class NavigationMenuComponent implements OnInit {

  constructor(
    private modalService: ModalService,
    private activatedRoute: ActivatedRoute,
  ) {
  }

  ngOnInit() {
    const query = this.activatedRoute.snapshot.queryParamMap;

    const action = query.get('action');
    const refNumber = query.get('refNumber');
    const carrierName = query.get('carrierName');

    if (action === 'parcel-create') {
      this.modalService.open(ParcelCreateComponent, {refNumber, carrierName});
    }
  }

  createParcel() {
    this.modalService.open(ParcelCreateComponent);
  }
}
