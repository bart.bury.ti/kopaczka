import {Component} from '@angular/core';

@Component({
  selector: 'app-wrapper-dashboard',
  templateUrl: './wrapper-dashboard.component.html',
  styleUrls: ['./wrapper-dashboard.component.scss']
})
export class WrapperDashboardComponent {}
