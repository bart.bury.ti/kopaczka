import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {WrapperDashboardComponent} from './wrapper-dashboard.component';
import {RouterOutlet} from "@angular/router";
import {NavigationModule} from "../../navigation/navigation.module";


@NgModule({
  declarations: [
    WrapperDashboardComponent
  ],
  imports: [
    CommonModule,
    RouterOutlet,
    NavigationModule,
  ]
})
export class WrapperDashboardModule {
}
