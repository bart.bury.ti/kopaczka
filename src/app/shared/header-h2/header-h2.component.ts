import {Component, Input} from '@angular/core';

@Component({
  selector: '[app-header-h2]',
  templateUrl: './header-h2.component.html',
  styleUrls: ['./header-h2.component.scss']
})
export class HeaderH2Component {
  @Input() color!: 'orange-100' | 'orange-200' | undefined;
}
