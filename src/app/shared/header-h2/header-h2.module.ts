import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderH2Component } from './header-h2.component';



@NgModule({
  declarations: [
    HeaderH2Component
  ],
  exports: [
    HeaderH2Component
  ],
  imports: [
    CommonModule
  ]
})
export class HeaderH2Module { }
