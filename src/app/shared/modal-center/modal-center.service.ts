import {Injectable, Injector} from '@angular/core';
import {ComponentType, Overlay, OverlayRef} from "@angular/cdk/overlay";
import {ComponentPortal} from "@angular/cdk/portal";
import {Subscription, take} from "rxjs";
import {MODAL_CENTER_EXTERNAL_DATA} from "./modal-center-token.injection";

@Injectable({
  providedIn: 'root',
})
export class ModalCenterService {
  private overlayRef!: OverlayRef;
  private subscription!: Subscription;

  constructor(private overlay: Overlay) {
  }

  open<T>(component: ComponentType<T>, data?: any): void {
    this.overlayRef = this.overlay.create({
      hasBackdrop: true,
      positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
    });

    const injector = Injector.create({
      providers: [{provide: MODAL_CENTER_EXTERNAL_DATA, useValue: data}],
    });

    const portal = new ComponentPortal(component, null, injector);

    this.overlayRef.attach(portal);

    this.subscription = this.overlayRef.backdropClick().pipe(take(1)).subscribe(() => this.close());
  }

  close(): void {
    this.overlayRef.detach();
    this.subscription.unsubscribe();
  }
}
