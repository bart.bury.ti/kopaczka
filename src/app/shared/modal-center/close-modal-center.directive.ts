import {Directive, HostListener} from '@angular/core';
import {ModalCenterService} from "./modal-center.service";

@Directive({
  selector: '[appCloseModalCenter]'
})
export class CloseModalCenterDirective {

  @HostListener('click')
  close() {
    this.modal.close();
  }

  constructor(private modal: ModalCenterService) {
  }
}
