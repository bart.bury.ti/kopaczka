import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {OverlayModule} from "@angular/cdk/overlay";
import {CloseModalCenterDirective} from './close-modal-center.directive';

@NgModule({
  declarations: [
    CloseModalCenterDirective,
  ],
  imports: [
    CommonModule,
    OverlayModule,
  ],
  exports: [
    CloseModalCenterDirective,
  ]
})
export class ModalCenterModule {
}
