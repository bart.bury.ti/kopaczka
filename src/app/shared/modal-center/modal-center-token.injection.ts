import {InjectionToken} from '@angular/core';

export const MODAL_CENTER_EXTERNAL_DATA = new InjectionToken('Modal external data injection token');
