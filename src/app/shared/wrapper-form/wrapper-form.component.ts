import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-wrapper-form',
  templateUrl: './wrapper-form.component.html',
  styleUrls: ['./wrapper-form.component.scss']
})
export class WrapperFormComponent {
  @Input() heading?: string;
}
