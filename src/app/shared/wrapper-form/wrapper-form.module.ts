import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {WrapperFormComponent} from './wrapper-form.component';
import {HeaderH2Module} from "../header-h2/header-h2.module";


@NgModule({
  declarations: [
    WrapperFormComponent
  ],
  exports: [
    WrapperFormComponent
  ],
  imports: [
    CommonModule,
    HeaderH2Module
  ]
})
export class WrapperFormModule {
}
