import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {WrapperAuthComponent} from './wrapper-auth.component';
import {RouterOutlet} from "@angular/router";
import {SvgModule} from "../svg/svg.module";


@NgModule({
  declarations: [
    WrapperAuthComponent
  ],
  imports: [
    CommonModule,
    RouterOutlet,
    SvgModule
  ]
})
export class WrapperAuthModule {
}
