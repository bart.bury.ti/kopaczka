import {Component, ElementRef, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-svg',
  templateUrl: './svg.component.html',
  styleUrls: ['./svg.component.scss']
})
export class SvgComponent implements OnInit {

  @Input() assetsSVG!: string;

  constructor(private elementRef: ElementRef) {
  }

  ngOnInit() {
    fetch(this.assetsSVG)
      .then(response => response.text())
      .then(text => this.elementRef.nativeElement.innerHTML = text)
  }
}
