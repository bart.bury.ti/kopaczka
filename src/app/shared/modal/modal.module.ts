import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {OverlayModule} from "@angular/cdk/overlay";
import {CloseModalDirective} from './close-modal.directive';

@NgModule({
  declarations: [
    CloseModalDirective,
  ],
  imports: [
    CommonModule,
    OverlayModule,
  ],
  exports: [
    CloseModalDirective,
  ]
})
export class ModalModule {
}
