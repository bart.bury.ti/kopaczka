import {Directive, HostListener} from '@angular/core';
import {ModalService} from "./modal.service";

@Directive({
  selector: '[appCloseModal]'
})
export class CloseModalDirective {

  @HostListener('click')
  close() {
    this.modal.close();
  }

  constructor(private modal: ModalService) {
  }
}
