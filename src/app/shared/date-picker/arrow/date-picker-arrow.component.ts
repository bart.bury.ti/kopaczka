import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-date-picker-arrow',
  templateUrl: './date-picker-arrow.component.html',
  styleUrls: ['./date-picker-arrow.component.scss']
})
export class DatePickerArrowComponent {
  @Input() direction: 'left' | 'right' | undefined;
}
