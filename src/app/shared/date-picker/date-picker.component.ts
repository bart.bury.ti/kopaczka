import {Component, forwardRef, OnInit} from '@angular/core';
import {getDaysInMonth, addMonths, subMonths, getDay, getDate} from 'date-fns';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";

@Component({
  selector: 'app-date-picker',
  templateUrl: './date-picker.component.html',
  styleUrls: ['./date-picker.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DatePickerComponent),
      multi: true,
    }
  ]
})

export class DatePickerComponent implements OnInit, ControlValueAccessor {
  protected headers: string[] = ['P', 'W', 'Ś', 'C', 'P', 'S', 'N'];
  protected daysOfCurrentMonth: number[] = [];
  protected date = new Date();
  protected visibleDate = new Date();
  protected startDate: number[] = [];

  onChange: any = () => {
  };
  onTouch: any = () => {
  };

  ngOnInit(): void {
    this.setDaysOfMonth();
  }

  writeValue(date: Date | null) {
    if (date === null) {
      return;
    }

    this.selectDate(getDate(date));
  }

  registerOnTouched(fn: any) {
    this.onTouch = fn;
  }

  registerOnChange(fn: any) {
    this.onChange = fn;
  }

  selectDate(day: number): void {
    this.date = new Date(this.visibleDate);
    this.date.setDate(day);

    this.onChange(this.date);
  }

  previousMonth(): void {
    this.visibleDate = subMonths(this.visibleDate, 1);
    this.setDaysOfMonth();
  }

  nextMonth(): void {
    this.visibleDate = addMonths(this.visibleDate, 1);
    this.setDaysOfMonth();
  }

  private setDaysOfMonth(): void {
    this.daysOfCurrentMonth = [];
    for (let i = 1; i <= getDaysInMonth(this.visibleDate); i++) {
      this.daysOfCurrentMonth.push(i);
    }

    this.startDate = [];
    for (let i = 0; i <= getDay(this.visibleDate); i++) {
      this.startDate.push(i);
    }
  }
}
