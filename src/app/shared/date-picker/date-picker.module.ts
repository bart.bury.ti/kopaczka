import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DatePickerComponent} from './date-picker.component';
import {DatePickerArrowComponent} from './arrow/date-picker-arrow.component';
import {HeaderH3Module} from "../header-h3/header-h3.module";
import {HeaderH2Module} from "../header-h2/header-h2.module";


@NgModule({
  declarations: [
    DatePickerComponent,
    DatePickerArrowComponent
  ],
  exports: [
    DatePickerComponent
  ],
  imports: [
    CommonModule,
    HeaderH3Module,
    HeaderH2Module
  ]
})
export class DatePickerModule {
}
