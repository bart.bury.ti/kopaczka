import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonSolidComponent } from './button-solid.component';



@NgModule({
    declarations: [
        ButtonSolidComponent
    ],
    exports: [
        ButtonSolidComponent
    ],
    imports: [
        CommonModule
    ]
})
export class ButtonSolidModule { }
