import {Component, Input} from '@angular/core';

@Component({
  selector: '[app-button-solid]',
  templateUrl: './button-solid.component.html',
  styleUrls: ['./button-solid.component.scss']
})
export class ButtonSolidComponent {
  @Input() size: 'xs' | undefined;
  @Input() color: 'primary' | 'secondary' | undefined;
}
