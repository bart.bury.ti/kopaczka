import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderH4Component } from './header-h4.component';



@NgModule({
    declarations: [
        HeaderH4Component
    ],
    exports: [
        HeaderH4Component
    ],
    imports: [
        CommonModule
    ]
})
export class HeaderH4Module { }
