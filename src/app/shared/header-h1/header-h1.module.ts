import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderH1Component } from './header-h1.component';



@NgModule({
    declarations: [
        HeaderH1Component
    ],
    exports: [
        HeaderH1Component
    ],
    imports: [
        CommonModule
    ]
})
export class HeaderH1Module { }
