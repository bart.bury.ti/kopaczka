import {Component, Input} from '@angular/core';

@Component({
  selector: '[app-header-h3]',
  templateUrl: './header-h3.component.html',
  styleUrls: ['./header-h3.component.scss']
})
export class HeaderH3Component {
  @Input() color!: 'orange-200' | 'grey-300' | undefined;
}
