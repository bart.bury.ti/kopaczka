import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderH3Component } from './header-h3.component';



@NgModule({
    declarations: [
        HeaderH3Component
    ],
    exports: [
        HeaderH3Component
    ],
    imports: [
        CommonModule
    ]
})
export class HeaderH3Module { }
