import {NgModule, isDevMode, APP_INITIALIZER} from '@angular/core';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {BrowserModule} from '@angular/platform-browser';

import {AppRouting} from './app.routing';
import {AppComponent} from './app.component';
import {ServiceWorkerModule} from '@angular/service-worker';
import {WrapperDashboardModule} from "./shared/wrapper-dashboard/wrapper-dashboard.module";
import {WrapperAuthModule} from "./shared/wrapper-auth/wrapper-auth.module";
import {AuthService} from "./auth/api/auth.service";
import {UserFacade} from "./user/user.facade";
import {authInitialize} from "./auth/auth.initialize";
import {Router} from "@angular/router";
import {AuthTokenInterceptor} from "./auth/auth-token.interceptor";
import {ApiConfigInterceptor} from "../config/api.config.interceptor";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRouting,
    WrapperDashboardModule,
    WrapperAuthModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: !isDevMode(),
      // Register the ServiceWorker as soon as the application is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000'
    })
  ],
  providers: [
    {
      provide: APP_INITIALIZER, useFactory: authInitialize, deps: [AuthService, UserFacade, Router], multi: true
    },
    {
      provide: HTTP_INTERCEPTORS, useClass: AuthTokenInterceptor, multi: true
    },
    {
      provide: HTTP_INTERCEPTORS, useClass: ApiConfigInterceptor, multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
