import {Injectable} from '@angular/core';
import {ReplaySubject} from "rxjs";
import {CognitoUserSession} from "amazon-cognito-identity-js";
import {User} from "./api/user.model";

@Injectable({
  providedIn: 'root'
})
export class UserFacade {
  private user$$ = new ReplaySubject<User>(1);
  user$ = this.user$$.asObservable();

  updateCognitoUser(cognitoUserSession: CognitoUserSession) {
    this.user$$.next({
      name: cognitoUserSession.getIdToken().payload['name'],
      cognito: cognitoUserSession
    })
  }

}
