import {CognitoUserSession} from "amazon-cognito-identity-js";

export interface User {
  name: string
  cognito: CognitoUserSession
}
