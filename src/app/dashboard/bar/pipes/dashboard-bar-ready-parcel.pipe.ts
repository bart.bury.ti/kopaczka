import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'dashboardBarReadyParcel'
})
export class DashboardBarReadyParcelPipe implements PipeTransform {

  transform(value: number): string {
    switch (value) {
      case 1:
        return value + ' paczka';
      case 2:
      case 3:
      case 4:
        return value + ' paczki';
      default:
        return value + ' paczek';
    }
  }
}
