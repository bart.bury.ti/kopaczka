import {Component} from '@angular/core';
import {ParcelFacade} from "../../parcel/parcel.facade";
import {map} from "rxjs";

@Component({
  selector: 'app-dashboard-bar',
  templateUrl: './dashboard-bar.component.html',
  styleUrls: ['./dashboard-bar.component.scss']
})
export class DashboardBarComponent {
  count$ = this.dashboardFacade.parcels$.pipe(
    map(packages => packages.filter(item => item.realizationStatus === 'READY_FOR_PICKUP')),
    map(readyForPickups => readyForPickups.length)
  )

  constructor(private dashboardFacade: ParcelFacade) {
  }
}
