import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardBarComponent } from './dashboard-bar.component';
import {SvgModule} from "../../shared/svg/svg.module";
import {ButtonSolidModule} from "../../shared/button-solid/button-solid.module";
import {HeaderH3Module} from "../../shared/header-h3/header-h3.module";
import { DashboardBarReadyParcelPipe } from './pipes/dashboard-bar-ready-parcel.pipe';



@NgModule({
  declarations: [
    DashboardBarComponent,
    DashboardBarReadyParcelPipe
  ],
  exports: [
    DashboardBarComponent
  ],
  imports: [
    CommonModule,
    SvgModule,
    ButtonSolidModule,
    HeaderH3Module
  ]
})
export class DashboardBarModule { }
