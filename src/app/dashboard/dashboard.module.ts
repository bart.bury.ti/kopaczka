import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {DashboardRouting} from './dashboard.routing';
import {DashboardHeaderModule} from "./header/dashboard-header.module";
import {DashboardBarModule} from "./bar/dashboard-bar.module";
import {FirstPackageModule} from "./components/first-package/first-package.module";
import {DashboardParcelsModule} from "./list/dashboard-parcels.module";
import {DashboardViewModule} from "./view/dashboard-view.module";

@NgModule({
  imports: [
    CommonModule,
    DashboardRouting,
    DashboardHeaderModule,
    DashboardBarModule,
    FirstPackageModule,
    DashboardViewModule,
    DashboardParcelsModule,
  ]
})
export class DashboardModule {
}
