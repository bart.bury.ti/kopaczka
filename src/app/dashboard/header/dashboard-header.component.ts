import {Component} from '@angular/core';
import {UserFacade} from "../../user/user.facade";

@Component({
  selector: 'app-dashboard-header',
  templateUrl: './dashboard-header.component.html',
  styleUrls: ['./dashboard-header.component.scss']
})
export class DashboardHeaderComponent {
  user$ = this.userFacade.user$;

  constructor(private userFacade: UserFacade) {
  }
}
