import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardHeaderComponent } from './dashboard-header.component';
import {HeaderH2Module} from "../../shared/header-h2/header-h2.module";
import {SvgModule} from "../../shared/svg/svg.module";



@NgModule({
    declarations: [
        DashboardHeaderComponent
    ],
    exports: [
        DashboardHeaderComponent
    ],
  imports: [
    CommonModule,
    HeaderH2Module,
    SvgModule
  ]
})
export class DashboardHeaderModule { }
