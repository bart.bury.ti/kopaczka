import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DashboardViewComponent} from "./view/dashboard-view.component";

export const dashboardRoutingPath = {
  parcel: 'parcel',
}

const routes: Routes = [
  {
    path: '', component: DashboardViewComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRouting {
}
