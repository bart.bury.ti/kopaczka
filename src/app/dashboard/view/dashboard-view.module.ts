import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DashboardViewComponent} from './dashboard-view.component';
import {DashboardHeaderModule} from "../header/dashboard-header.module";
import {DashboardBarModule} from "../bar/dashboard-bar.module";
import {DashboardParcelsModule} from "../list/dashboard-parcels.module";
import {FirstPackageModule} from "../components/first-package/first-package.module";
import {DatePickerModule} from "../../shared/date-picker/date-picker.module";


@NgModule({
  declarations: [
    DashboardViewComponent
  ],
  imports: [
    CommonModule,
    DashboardHeaderModule,
    DashboardBarModule,
    DashboardParcelsModule,
    FirstPackageModule
  ]
})
export class DashboardViewModule {
}
