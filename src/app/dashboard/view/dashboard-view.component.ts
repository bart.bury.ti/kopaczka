import {Component} from '@angular/core';
import {ParcelFacade} from "../../parcel/parcel.facade";
import {map} from "rxjs";

@Component({
  selector: 'app-dashboard-view',
  templateUrl: './dashboard-view.component.html',
  styleUrls: ['./dashboard-view.component.scss']
})
export class DashboardViewComponent {
  data$ = this.dashboardFacade.parcels$.pipe(
    map(parcels => {
      return {
        anyParcels: !!parcels.length,
        readyForPickup: !!parcels.filter(parcel => parcel.realizationStatus === 'READY_FOR_PICKUP').length
      }
    })
  );

  constructor(private dashboardFacade: ParcelFacade) {
  }
}
