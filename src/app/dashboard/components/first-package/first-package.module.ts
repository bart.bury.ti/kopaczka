import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FirstPackageComponent } from './first-package.component';
import {HeaderH1Module} from "../../../shared/header-h1/header-h1.module";
import {SvgModule} from "../../../shared/svg/svg.module";



@NgModule({
  declarations: [
    FirstPackageComponent
  ],
  exports: [
    FirstPackageComponent
  ],
  imports: [
    CommonModule,
    HeaderH1Module,
    SvgModule
  ]
})
export class FirstPackageModule { }
