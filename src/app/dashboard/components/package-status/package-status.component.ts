import {Component, Input} from '@angular/core';
import {Parcel} from "../../../parcel/api/parcel.model";

@Component({
  selector: 'app-package-status',
  templateUrl: './package-status.component.html',
  styleUrls: ['./package-status.component.scss']
})
export class PackageStatusComponent {
  @Input() parcel!: Parcel;
}
