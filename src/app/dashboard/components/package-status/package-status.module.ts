import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PackageStatusComponent } from './package-status.component';
import {HeaderH4Module} from "../../../shared/header-h4/header-h4.module";



@NgModule({
    declarations: [
        PackageStatusComponent
    ],
    exports: [
        PackageStatusComponent
    ],
    imports: [
        CommonModule,
        HeaderH4Module
    ]
})
export class PackageStatusModule { }
