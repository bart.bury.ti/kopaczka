import {Component, Input} from '@angular/core';
import {Parcel} from "../../../parcel/api/parcel.model";

@Component({
  selector: 'app-packages-package',
  templateUrl: './parcels-parcel.component.html',
  styleUrls: ['./parcels-parcel.component.scss']
})
export class ParcelsParcelComponent {
  @Input() parcel!: Parcel;
}
