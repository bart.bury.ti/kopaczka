import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ParcelsParcelComponent} from './parcels-parcel.component';
import {HeaderH3Module} from "../../../shared/header-h3/header-h3.module";
import {PackageStatusModule} from "../../components/package-status/package-status.module";
import {FullStreetAddressPipe} from "../../pipes/full-street-address.pipe";

@NgModule({
  declarations: [
    ParcelsParcelComponent,
  ],
  exports: [
    ParcelsParcelComponent,
  ],
  imports: [
    CommonModule,
    HeaderH3Module,
    PackageStatusModule,
    FullStreetAddressPipe
  ]
})
export class ParcelsParcelModule {
}
