import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ParcelsSearchComponent} from './parcels-search.component';
import {SvgModule} from "../../../shared/svg/svg.module";
import {ReactiveFormsModule} from "@angular/forms";


@NgModule({
  declarations: [
    ParcelsSearchComponent
  ],
  exports: [
    ParcelsSearchComponent
  ],
  imports: [
    CommonModule,
    SvgModule,
    ReactiveFormsModule
  ]
})
export class ParcelsSearchModule {
}
