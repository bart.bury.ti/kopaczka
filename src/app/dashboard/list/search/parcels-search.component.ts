import {Component} from '@angular/core';
import {FormControl} from "@angular/forms";

@Component({
  selector: 'app-packages-search',
  templateUrl: './parcels-search.component.html',
  styleUrls: ['./parcels-search.component.scss']
})
export class ParcelsSearchComponent {
  searchControl = new FormControl('', {nonNullable: true});
}
