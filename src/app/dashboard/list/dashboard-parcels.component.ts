import {AfterViewInit, Component, OnDestroy, ViewChild} from '@angular/core';
import {ParcelFacade} from "../../parcel/parcel.facade";
import {dashboardRoutingPath} from "../dashboard.routing";
import {ParcelsSearchComponent} from "./search/parcels-search.component";
import {map, Observable, of, Subscription, switchMap, take, tap} from "rxjs";
import {Parcel} from "../../parcel/api/parcel.model";

@Component({
  selector: 'app-dashboard-packages',
  templateUrl: './dashboard-parcels.component.html',
  styleUrls: ['./dashboard-parcels.component.scss']
})
export class DashboardParcelsComponent implements AfterViewInit, OnDestroy {
  private subscription = new Subscription();

  searching: boolean = false;

  packageRouter = dashboardRoutingPath.parcel;
  anyPackageExist$ = this.dashboardFacade.parcels$.pipe(
    map(packages => !!packages.length)
  );
  packages$ = this.dashboardFacade.parcels$;

  @ViewChild(ParcelsSearchComponent) search!: ParcelsSearchComponent;

  constructor(private dashboardFacade: ParcelFacade) {
  }

  ngAfterViewInit() {
    this.subscription.add(this.enableSearching());
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  private enableSearching() {
    return this.search.searchControl.valueChanges.pipe(
      tap(() => this.searching = true),
      switchMap(search => this.dashboardFacade.parcels$.pipe(
        take(1),
        map(packages => packages.filter(item => item.refNumber.toLowerCase().includes(search.toLowerCase()) || item.sender.companyName.toLowerCase().includes(search.toLowerCase()))),
      )),
      tap(result => this.packages$ = of(result))
    ).subscribe();
  }
}
