import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DashboardParcelsComponent} from './dashboard-parcels.component';
import {ParcelsSearchModule} from "./search/parcels-search.module";
import {RouterLink} from "@angular/router";
import {HeaderH2Module} from "../../shared/header-h2/header-h2.module";
import {HeaderH1Module} from "../../shared/header-h1/header-h1.module";
import {ParcelsParcelModule} from "./parcel/parcels-parcel.module";


@NgModule({
  declarations: [
    DashboardParcelsComponent
  ],
  exports: [
    DashboardParcelsComponent
  ],
  imports: [
    CommonModule,
    ParcelsSearchModule,
    RouterLink,
    HeaderH2Module,
    HeaderH1Module,
    ParcelsParcelModule
  ]
})
export class DashboardParcelsModule {
}
