import {Pipe, PipeTransform} from '@angular/core';
import {Parcel} from "../../parcel/api/parcel.model";

@Pipe({
  name: 'parcelDimensions',
  standalone: true
})
export class ParcelDimensionsPipe implements PipeTransform {

  transform(dimensions: Parcel['dimensions']): string {
    return `${dimensions.width}x${dimensions.height}x${dimensions.length}`;
  }

}
