import {Pipe, PipeTransform} from '@angular/core';
import {Address} from "../../parcel/api/parcel.model";

@Pipe({
  name: 'fullStreetAddress',
  standalone: true,
})
export class FullStreetAddressPipe implements PipeTransform {
  transform(address: Address): string {
    return `${address.street} ${address.streetNumber}/${address.flatNumber}`;
  }
}
